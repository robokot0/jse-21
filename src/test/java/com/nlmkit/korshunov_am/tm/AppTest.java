package com.nlmkit.korshunov_am.tm;

import static org.junit.Assert.assertTrue;

import com.nlmkit.korshunov_am.tm.entity.Project;
import com.nlmkit.korshunov_am.tm.entity.Task;
import com.nlmkit.korshunov_am.tm.exceptions.ProjectNotFoundException;
import com.nlmkit.korshunov_am.tm.exceptions.TaskNotFoundException;
import com.nlmkit.korshunov_am.tm.exceptions.WrongArgumentException;
import com.nlmkit.korshunov_am.tm.service.ProjectService;
import com.nlmkit.korshunov_am.tm.service.ProjectTaskService;
import com.nlmkit.korshunov_am.tm.service.TaskService;
import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue() throws ProjectNotFoundException, TaskNotFoundException, WrongArgumentException {
        App.addTestData();
        final Task task = TaskService.getInstance().findByIndex(0,true);
        System.out.println(task);
        final Project project =
                ProjectService.getInstance().findByIndex(0,true);
        System.out.println(project);
        ProjectTaskService.getInstance().addTaskToProject(project.getId(),task.getId());
        System.out.println(ProjectTaskService.getInstance().findAllByProjectId(project.getId()));
        ProjectTaskService.getInstance().removeTaskFromProject(project.getId(),task.getId());
        System.out.println(ProjectTaskService.getInstance().findAllByProjectId(project.getId()));
        assertTrue( true );
    }
}
