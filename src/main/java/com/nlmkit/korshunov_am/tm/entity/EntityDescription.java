package com.nlmkit.korshunov_am.tm.entity;

public interface EntityDescription {
    /**
     * Получить описание
     * @return описание
     */
    String getDescription();

    /**
     * Задлать описание
     * @param description описание
     */
    void setDescription(final String description);
}
