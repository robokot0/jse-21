package com.nlmkit.korshunov_am.tm.repository;

import com.nlmkit.korshunov_am.tm.entity.Command;

import java.util.ArrayList;
import java.util.List;

public class CommandHistoryRepository {
    /**
     * Приватный конструктор по умолчанию
     */
    private CommandHistoryRepository(){
    }
    /**
     * Единственный экземпляр объекта CommandHistoryRepository
     */
    private static CommandHistoryRepository instance = null;

    /**
     * Получить единственный экземпляр объекта CommandHistoryRepository
     * @return единственный экземпляр объекта CommandHistoryRepository
     */
    public static CommandHistoryRepository getInstance(){
        if (instance == null){
            instance = new CommandHistoryRepository();
        }
        return instance;
    }

    /**
     * Список для хранения истории комманд
     */
    private final ArrayList<Command> commandhistory = new ArrayList<>();
    private Command lastcommand;
    /**
     * Добавляет комманду в историю
     * @param command комманда
     */
    public void AddCommandToHistory(final String command){
        lastcommand=new Command(command);
        commandhistory.add(lastcommand);
        if(commandhistory.size()>10) commandhistory.remove(0);
    }

    /**
     * Добавляет параметр комманды в историю
     * @param commandParameter параметр комманды
     * @param parameterValue значение параметра
     */
    public void AddCommandParameterToLastCommand(final String commandParameter,final String parameterValue){
        lastcommand.addCommandText(commandParameter);
        lastcommand.addCommandText(parameterValue);
    }

    /**
     * Добавляет результат исполнения комманды
     * @param commandResult результат исполнения комманды
     */
    public void AddCommandResultToLastCommand(final String commandResult){
        lastcommand.addCommandText(commandResult);
    }
    /**
     * Получить список всех комманд из истории
     * @return список комманд
     */
    public List<Command> findAll() {
        return commandhistory;
    }


}
