package com.nlmkit.korshunov_am.tm.repository;

import com.nlmkit.korshunov_am.tm.entity.Project;
import com.nlmkit.korshunov_am.tm.entity.Task;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Репозитарий задач
 */
public class TaskRepostory  extends AbstractRepository <Task> {
    /**
     * Приватный конструктор по умолчанию
     */
    private TaskRepostory(){
    }
    /**
     * Единственный экземпляр объекта TaskRepostory
     */
    private static TaskRepostory instance = null;

    /**
     * Получить единственный экземпляр объекта TaskRepostory
     * @return единственный экземпляр объекта TaskRepostory
     */
    public static TaskRepostory getInstance(){
        if (instance == null){
            instance = new TaskRepostory();
        }
        return instance;
    }

    /**
     * Создать задачу
     * @param name имя
     * @param userId ид пользователя
     * @return задача
     */
    public Task create(String name,final Long userId) {
        final Task task = new Task(name,userId);
        return create(task);
    }

    /**
     * Создать задачу
     * @param name имя
     * @param description описание
     * @param userId ид пользователя
     * @return задача
     */
    public Task create(final String name,final String description,final Long userId) {
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        task.setUserId(userId);
        return create(task);
    }


    /**
     * Ищем задачу по идентификатору проекта и по идентификатору задачи
     * @param projectId идентификатор проекта
     * @param id идентификатор задачи
     * @return задача
     */
    public Task findByProjectIdAndId(final Long projectId,final Long id){
        final Task task = findById(id);
        if (task == null) return null;
        final Long idProject = task.getProjectId();
        if (idProject == null) return null;
        if (!idProject.equals(projectId))  return null;
        return task;
    }
    /**
     * Ищем задачу по идентификатору проекта идентификатору задачи и пользователю.
     * @param projectId идентификатор проекта
     * @param id идентификатор задачи
     * @param userId ид пользователя
     * @return задача
     */
    public Task findByProjectIdAndId(final Long projectId,final Long id,final Long userId){
        final Task task= findByProjectIdAndId(projectId,id);
        if (task == null) return null;
        if (task.getUserId().equals(userId)) return task;
        return null;
    }

    /**
     * Найти по идентификатору проект и добавить к нему задачу
     * @param projectId идентификатор проекта
     * @param taskId идентификатор задачи
     * @return задача null если задачи не найдено
     */
    public Task findAddByProjectId(final Long projectId,final Long taskId) {
        final Task task = findByProjectIdAndId(projectId,taskId);
        if (task == null) return null;
        task.setProjectId(projectId);
        return task;
    }

    /**
     * Найти по идентификатору проект и добавить к нему задачу
     * задача должны быть указанного пользователя
     * @param projectId идентификатор проекта
     * @param taskId идентификатор задачи
     * @param userId ид пользователя
     * @return задача null если задачи не найдено
     */
    public Task findAddByProjectId(final Long projectId,final Long taskId,final Long userId) {
        Task task = findById(taskId,userId);
        if(task==null) return null;
        task.setProjectId(projectId);
        return task;
    }

    /**
     * Получить список задач проекта
     * @param projectId идентрификатор проектв
     * @return списрк задач
     */
    public List<Task> findAllByProjectId(final Long projectId) {
        final List<Task> result = new ArrayList<>();
        for (final Task task: findAll()) {
            final Long idProject = task.getProjectId();
            if (idProject == null) continue;
            if (projectId.equals(idProject)) result.add(task);
        }
        return result;
    }

    /**
     * Получить список задач проекта с учетом пользователя
     * @param projectId идентрификатор проектв
     * @param userId ид пользователя
     * @return списрк задач
     */
    public List<Task> findAllByProjectId(final Long projectId,final Long userId) {
        final List<Task> result = new ArrayList<>();
        for (Map.Entry<String, List<Task>> entry : entityes.entrySet()) {
            List<Task> taskList = entry.getValue();
            for (final Task task: taskList){
                final Long idProject = task.getProjectId();
                if (idProject == null) continue;
                if (!task.getUserId().equals(userId))continue;
                if (projectId.equals(idProject)) result.add(task);
            }
        }
        return result;
    }
}
