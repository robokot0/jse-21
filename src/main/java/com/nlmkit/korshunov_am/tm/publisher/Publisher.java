package com.nlmkit.korshunov_am.tm.publisher;

import com.nlmkit.korshunov_am.tm.listener.Listener;

public interface Publisher {
    /**
     * Доабвить слушатель комманд в список оповещения
     * @param listener слушатель комманд
     */
    Publisher addListener(Listener listener);

    /**
     * Удалить слушателя комманд
     * @param listener слушатель комманд
     */
    void deleteListener(Listener listener);
    void notifyListener(String command);
    void readAndExecuteCommand();
}
