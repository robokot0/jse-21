package com.nlmkit.korshunov_am.tm;

import com.nlmkit.korshunov_am.tm.entity.*;
import com.nlmkit.korshunov_am.tm.enumerated.*;
import com.nlmkit.korshunov_am.tm.exceptions.MessageException;
import com.nlmkit.korshunov_am.tm.listener.*;
import com.nlmkit.korshunov_am.tm.publisher.*;
import com.nlmkit.korshunov_am.tm.repository.*;
import com.nlmkit.korshunov_am.tm.service.*;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

/**
 *  Тестовое приложение
 */
public class App {
    /**
     * Логгер
     */
    final static Logger logger = LogManager.getLogger(App.class.getName());
    /**
     * Источник комманд
     */
    static Publisher publisher = new PublisherImpl()
            .addListener(SystemListener.getInstance())
            .addListener(CommandHistoryListener.getInstance())
            .addListener(ProjectListener.getInstance())
            .addListener(TaskListener.getInstance())
            .addListener(UserListener.getInstance());

    public static void addTestData(){
        final ProjectRepository projectRepository=ProjectRepository.getInstance();
        final UserRepository userRepository=UserRepository.getInstance();
        final TaskRepostory taskRepostory=TaskRepostory.getInstance();
        final ProjectTaskService projectTaskService=ProjectTaskService.getInstance();
        final UserService userService=UserService.getInstance();

        try {

            final User user1=userRepository.create("USER",Role.valueOf("USER"),"FN","SN","MN",userService.getStringHash("123"));
            final User user2=userRepository.create("USER1",Role.valueOf("USER"),"FN1","SN1","MN1",userService.getStringHash("123"));
            final User admin=userRepository.create("ADMIN",Role.valueOf("ADMIN"),"FN","SN","MN",userService.getStringHash("123"));

            final Project project1 = projectRepository.create("P2","D1",user1.getId());
            final Project project2 = projectRepository.create("P1","D2",user2.getId());

            final Task task1 = taskRepostory.create("T3","D1",user1.getId());
            final Task task2 = taskRepostory.create("T2","D2",user2.getId());
            final Task task3 = taskRepostory.create("T1","D3",user1.getId());

            projectTaskService.addTaskToProject(project1.getId(),task1.getId());
            projectTaskService.addTaskToProject(project2.getId(),task2.getId());

        }
        catch (MessageException e) {
            logger.error("Ошибка создания тестовых данных " + e.getMessage());
        }
    }
    /**
     * Точка входа
     * @param args Аргументы коммандной строки.
     */
    public static void main(String[] args) {
        addTestData();
        if (args != null)
            if (args.length >=1) {
                final String param = args[0];
                publisher.notifyListener(param);
            }
        publisher.readAndExecuteCommand();
    }

}
