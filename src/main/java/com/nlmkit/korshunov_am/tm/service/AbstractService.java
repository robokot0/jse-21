package com.nlmkit.korshunov_am.tm.service;

import com.nlmkit.korshunov_am.tm.entity.User;
import com.nlmkit.korshunov_am.tm.enumerated.Role;
import com.nlmkit.korshunov_am.tm.exceptions.WrongArgumentException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

public class AbstractService{
    /**
     * Логгер
     */
    final Logger logger = LogManager.getLogger(this.getClass().getName());
    /**
     * объект для ввода параметров коммад
     */
    protected final Scanner scanner = new Scanner(System.in);
    /**
     * Репозиторий истории комманд
     */
    protected CommandHistoryService commandHistoryService;
    /**
     * Конструктор
     */
    public AbstractService(CommandHistoryService commandHistoryService) {
        this.commandHistoryService = commandHistoryService;
    }

    /**
     * Текущий пользователь
     */
    private User user=null;

    /**
     * получить getСommandHistoryService
     * @return СommandHistoryService
     */
    public CommandHistoryService getСommandHistoryService() {
        return commandHistoryService;
    }

    /**
     * Ввод строкового параметра
     * @param parameterName Имя параметра
     * @return Введенное значение
     */
    public String EnterStringCommandParameter(final String parameterName){
        logger.info("Please enter "+parameterName+": ");
        final String parametervalue = scanner.nextLine();
        commandHistoryService.AddCommandParameterToLastCommand(parameterName,parametervalue);
        return parametervalue;
    }
    /**
     * Ввод строкового параметра пароля (без отображения в истории пароля)
     * @param parameterName Имя параметра
     * @return Введенное значение
     */
    public String EnterPasswordCommandParameter(final String parameterName){
        logger.info("Please enter "+parameterName+": ");
        final String parametervalue = scanner.nextLine();
        commandHistoryService.AddCommandParameterToLastCommand(parameterName,"******");
        return parametervalue;
    }
    /**
     * Ввод int параметра
     * @param parameterName Имя параметра
     * @return Введенное значение
     */
    public Integer EnterIntegerCommandParameter(final String parameterName) throws WrongArgumentException {
        logger.info("Please enter "+parameterName+": ");
        final String parametervalue = scanner.nextLine();
        commandHistoryService.AddCommandParameterToLastCommand(parameterName,parametervalue);
        if (parametervalue.isEmpty()) throw new WrongArgumentException("Не задано значение парамера "+parameterName);
        final int result = Integer.parseInt(parametervalue);
        return result;
    }
    /**
     * Ввод long параметра
     * @param parameterName Имя параметра
     * @return Введенное значение
     */
    public Long EnterLongCommandParameter(final String parameterName) throws WrongArgumentException {
        logger.info("Please enter "+parameterName+": ");
        final String parametervalue = scanner.nextLine();
        commandHistoryService.AddCommandParameterToLastCommand(parameterName,parametervalue);
        if (parametervalue.isEmpty()) throw new WrongArgumentException("Не задано значение парамера "+parameterName);
        final long result = Long.parseLong(parametervalue);
        return result;
    }

    /**
     * Показать результат исполнения комманды и добавить в историю
     * @param result результат
     */
    public void ShowResult(final String result){
        logger.info(result);
        commandHistoryService.AddCommandResultToLastCommand(result);
    }
    /**
     * Установить текущего пользователя
     * @return текущий пользователь
     */
    public User getUser() {
        return user;
    }
    /**
     * Получить текущего пользователя
     * @param user пользователь
     */
    public void setUser(User user) {
        this.user = user;
    }
    /**
     * Проверка что пользователь аутентифицировался
     * @return true пользовтаель аутентифицировался
     * пользователь не аутентифицировался
     */
    public boolean testAuthUser(){
        if (this.user==null){
            logger.info("Please auth: ");
            ShowResult("[FAIL]");
            return false;
        }
        return true;
    }

    /**
     * Проверка что пользователь аутентифицировался и это администратор
     * @return true пользовтаель аутентифицировался и это администратор false
     * пользователь не аутентифицировался или это не администратор
     */
    public boolean testAdminUser(){
        if(!testAuthUser())
            return false;
        if (this.user.getRole() != Role.ADMIN){
            logger.info("Please auth admin user: ");
            ShowResult("[FAIL]");
            return false;
        }
        return true;
    }

}
