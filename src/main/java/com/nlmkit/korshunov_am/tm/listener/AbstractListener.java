package com.nlmkit.korshunov_am.tm.listener;

import com.nlmkit.korshunov_am.tm.service.CommandHistoryService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class AbstractListener {
    /**
     * Логгер
     */
    final Logger logger = LogManager.getLogger(this.getClass().getName());
    /**
     * Сервис истории комманд
     */
    final CommandHistoryService commandHistoryService = CommandHistoryService.getInstance();

}
