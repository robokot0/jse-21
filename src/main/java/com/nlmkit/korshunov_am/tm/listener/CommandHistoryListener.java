package com.nlmkit.korshunov_am.tm.listener;

import com.nlmkit.korshunov_am.tm.service.CommandHistoryService;

import static com.nlmkit.korshunov_am.tm.TerminalConst.*;

public class CommandHistoryListener extends AbstractListener implements Listener {
    CommandHistoryService commandHistoryService = CommandHistoryService.getInstance();
    /**
     * Приватный конструктор по умолчанию
     */
    private CommandHistoryListener(){
        super();
    }
    /**
     * Единственный экземпляр объекта CommandHistoryListener
     */
    private static CommandHistoryListener instance = null;

    /**
     * Получить единственный экземпляр объекта CommandHistoryListener
     * @return единственный экземпляр объекта CommandHistoryListener
     */
    public static CommandHistoryListener getInstance(){
        if (instance == null){
            instance = new CommandHistoryListener();
        }
        return instance;
    }

    @Override
    public int notify(String command) {
        switch (command) {
            case SHORT_COMMAND_HISTORY_VIEW:
            case COMMAND_HISTORY_VIEW: return commandHistoryService.listCommandHistory();
            case SHORT_HELP:
            case HELP:return commandHistoryService.displayHelp();
            default:return -1;
        }
    }
}
