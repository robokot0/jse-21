package com.nlmkit.korshunov_am.tm.listener;

public interface Listener {
    int notify(String command);
}
